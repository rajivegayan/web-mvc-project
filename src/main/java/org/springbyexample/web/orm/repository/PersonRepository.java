package org.springbyexample.web.orm.repository;

import org.springbyexample.web.orm.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonRepository extends JpaRepository<Person, Integer> {
    
}
